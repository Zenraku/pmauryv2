const express = require('express'),
      router  = express.Router();

const apiController = require('../controllers/apiController.js');

router.get('/forgotApiKey', apiController.getForgotKey);

module.exports = router;