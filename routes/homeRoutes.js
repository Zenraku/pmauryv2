const express = require('express'),
      router  = express.Router();

router.get('/', (req, res) => {
    res.sendFile(__dirname + '/dist/index.html');
});

router.get('/secret', (req, res) => {
    res.json({success: true})
});

module.exports = router;