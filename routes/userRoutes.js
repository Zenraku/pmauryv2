const express = require('express'),
      router  = express.Router();

const { check, validationResult } = require('express-validator');
const userController              = require('../controllers/userController.js');

router.post('/signup', [
    //Email
    check('email')
    .isEmail().withMessage('It must be a email.')
    .isLength({ min: 4, max: 150 })
    .withMessage('Email address must be between 4-100 characters long, please try again.')
    .trim(),
    //Password
    check('password')
    .isLength({ min: 8, max: 100})
    .withMessage('Password must be between 8-100 characters long.')
    .matches(/^(?=.*[\d])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[\w!@#$%^&*]{8,}$/, 'i')
    .withMessage('Password must include one lowercase character, one uppercase character, a number, and a special character.')
    .trim(),
    //confirmPassword
    check('confirmPassword')
    .isLength({ min: 8, max: 100})
    .withMessage('Password must be between 8-100 characters long.')
    .custom((value, {req, loc, path }) => {
        if(req.body.password !== req.body.confirmPassword) {
            throw new Error("Passwords don't match");
        } else { return true; }
    })
    .trim()
], userController.postSignUp);

router.post('/login', [
    //Email
    check('email')
    .isEmail().withMessage('It must be a email.')
    .trim(),
    //Password
    check('password')
    .isLength({ min: 1})
    .withMessage('Need Password.')
    .trim(),
], userController.postLogin);

router.post('/forgotPassword', [
    //Email
    check('email')
    .isEmail().withMessage('It must be a email.')
    .trim()
], userController.postForgotPassword);

module.exports = router;