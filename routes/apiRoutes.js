const express = require('express'),
      router  = express.Router();

const apiController = require('../controllers/apiController.js');

router.get('/docs', apiController.getDocs);

router.get('/countries', apiController.getCountries);

router.get('/states', apiController.getStates);

router.get('/cities', apiController.getCities);

router.get('/key', apiController.getKey);

router.delete('/deleteKey', apiController.deleteKey);

module.exports = router;