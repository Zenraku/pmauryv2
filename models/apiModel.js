const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const apiSchema = new Schema({ 
    idUser: {
        type    : String,
        unique  : true,
        required: true
    },
    arrKeyApi: [{
        name: String,
        key : String
    }]
});

module.exports = mongoose.model('Api', apiSchema);
