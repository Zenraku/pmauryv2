# Site Web pmaury.fr

Refonte complete du site pmaury.fr (V2)<br>
**TODO** Penser a une V3 niveau architecture + front pour 2022/2023

## Pour commencer
+ https://www.pmaury.fr

### Pré-requis

Backend : 
- [express](https://www.npmjs.com/package/express)
- [helmet](https://www.npmjs.com/package/helmet)
- [expressJwt](https://www.npmjs.com/package/express-jwt)
- [mongoose](https://www.npmjs.com/package/mongoose)
- [bodyParser](https://www.npmjs.com/package/body-parser)
- [cookieParser](https://www.npmjs.com/package/cookie-parser)
- [session](https://www.npmjs.com/package/express-session)
- [listEndpoints](https://www.npmjs.com/package/express-list-endpoints)
- [bcrypt](https://www.npmjs.com/package/bcryptjs)
- [express-validator](https://www.npmjs.com/package/express-validator)
- [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)
- [core-js](https://www.npmjs.com/package/core-js)
- [crypto-js](https://www.npmjs.com/package/crypto-js)
- [express-locale](https://www.npmjs.com/package/express-locale)
- [mongoose](https://www.npmjs.com/package/mongoose)
- [njwt](https://www.npmjs.com/package/njwt)
- [node-polyglot](https://www.npmjs.com/package/node-polyglot)
- [node-sass](https://www.npmjs.com/package/node-sass)
- [nodemailer-sendgrid-transport](https://www.npmjs.com/package/nodemailer-sendgrid-transport)
- [nodemailer](https://www.npmjs.com/package/nodemailer)

Frontend :
- [i18n](https://www.npmjs.com/package/i18n)
- [vue-router](https://www.npmjs.com/package/vue-router)
- [vue-axios](https://www.npmjs.com/package/vue-axios)
- [vue-cookies](https://www.npmjs.com/package/vue-cookies)
- [jquery](https://www.npmjs.com/package/jquery)
- [lottie-web](https://www.npmjs.com/package/lottie-web)
- [@fortawesome/fontawesome-svg-core](https://www.npmjs.com/package/@fortawesome/fontawesome-svg-core)
- [@fortawesome/free-regular-svg-icons](https://www.npmjs.com/package/@fortawesome/free-regular-svg-icons)
- [@fortawesome/free-solid-svg-icons](https://www.npmjs.com/package/@fortawesome/free-solid-svg-icons)
- [@fortawesome/vue-fontawesome](https://www.npmjs.com/package/@fortawesome/vue-fontawesome)
- [animate.css](https://www.npmjs.com/package/animate.css)
- [detect-browser](https://www.npmjs.com/package/detect-browser)
- [sass-loader](https://www.npmjs.com/package/sass-loader)
- [vue-directive-waves](https://www.npmjs.com/package/vue-directive-waves)
- [vue-flickity](https://www.npmjs.com/package/vue-flickity)
- [vue-js-modal](https://www.npmjs.com/package/vue-js-modal)
- [vuedraggable](https://www.npmjs.com/package/vuedraggable)

## Fabriqué avec

* [Vue.Js](https://vuejs.org/) - Framework Javascript (front-end)
* [Node.Js](https://nodejs.org/) - Plateforme Javascript (back-end)
* [Express.Js](https://expressjs.com/fr/) - Framework Javascript (back-end)
* [Visual Studio Code](https://code.visualstudio.com/) - Editeur de textes


## Versions

**Dernière version stable :** 2.0<br/>
**Dernière version :** 2.0<br/>
Liste des versions : <br/>[Cliquer pour afficher](https://gitlab.com/Zenraku/pmauryv2/-/tags)<br><br>

La V1 est dans un autre projet.

## Auteurs

* **Pierre Maury** _alias_ [@Zenraku](https://github.com/Zenraku)

