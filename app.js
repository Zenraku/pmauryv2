//Import 
//Dependancies
const express       = require('express');
const helmet        = require('helmet');
const http          = require('http');
const expressJwt    = require('express-jwt');
const mongoose      = require('mongoose');
const bodyParser    = require('body-parser');
const cookeParser   = require('cookie-parser');
const session       = require('express-session');
const listEndpoints = require('express-list-endpoints');

//Controller
const errorController = require('./controllers/errorController.js');
const importData      = require('./controllers/importDataController.js');


//Database
const secuGlobalVariable = require('./data/secret/DBConfig.js'),
      mongoDbUri         = 'mongodb+srv://'+ secuGlobalVariable.user +
                           ':'+ secuGlobalVariable.password +
                           '@cluster0-hvmz2.gcp.mongodb.net/'+ secuGlobalVariable.dbName +
                           '?retryWrites=true&w=majority';

//Other
const PORT           = process.env.PORT;
const path           = require('path');
const variableSecret = require('./data/secret/variableSecret.js');
const config         = require('./data/config/config.js');
const secretJwt      = variableSecret.secretJWT;
const secretSession  = variableSecret.secretJWT;


//Instantiate server 
const app = express();

//Middleware
app.use(express.static(path.join(__dirname, '/public')));
app.use(helmet());
app.use(expressJwt({ credentialsRequired: true, secret: secretJwt, requestProperty: 'user' }).unless({ path: config.publicRoutes}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookeParser());
app.use(session({ secret: secretSession, 
                  resave           : false,
                  saveUninitialized: true
                }));
app.use((req, res, next) => {
    if(!req.session.initialised) {
        req.session.initialised = true;
        req.session.idUser = '';
    }
    next();
});

//Import Routes
const userRoutes      = require('./routes/userRoutes.js');
const homeRoutes      = require('./routes/homeRoutes.js');
const apiRoutes       = require('./routes/apiRoutes.js');
const apiGlobalRoutes = require('./routes/apiGlobalRoutes.js');

//Routes
app.use(homeRoutes);
app.use(userRoutes);
app.use('/api/world_list', apiRoutes);
app.use(apiGlobalRoutes);

//Error Handling
app.use(errorController.getError);
 
//Connexion
mongoose
.connect(mongoDbUri, { useCreateIndex: true,
                       useUnifiedTopology: true,
                       useNewUrlParser   : true },
(err, db) => {
    //Import File Data
    importData.importData(err, db); 
    //Set Dynamic Public Routes
    config.setPublicRoutes(listEndpoints(app));   
})
.then(() => {
    console.log('Database connected !')
    //Create server 
    http.createServer(app, console.log(`Server Listen on port ${PORT}`)).listen(PORT);
})
.catch(err => {
    console.log(`Database Connection Error : ${err.message}`)
});
