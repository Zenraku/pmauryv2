const UserModel                   = require('../models/userModel.js');
const SecuriteUser                = require('../class/securiteUserClass.js');
const Securite                    = require('../class/securiteClass.js');
const LoginUser                   = require('../class/loginUserClass.js');
const Email                       = require('../class/emailClass.js');
const { check, validationResult } = require('express-validator');
const securite                    = new Securite();

exports.postSignUp = (req, res, next) => {
    const email           = req.body.email;
    const password        = req.body.password;
    const confirmPassword = req.body.confirmPassword;
    const dateCreate      = new Date().getTime();
    let   securiteUser    = new SecuriteUser(email, password, confirmPassword, dateCreate);
    
    securiteUser.checkEmpty().then((bool) => {
        if(bool == true) {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
              return res.status(400).json({ errors: errors.array() })
            } else {
                securite.encrypt(securiteUser.password).then((result) => {
                    const User = new UserModel({
                        email     : securiteUser.email,
                        password  : result,
                        dateCreate: securiteUser.dateCreate
                    });

                    User.save()
                    .then(result => {
                        let loginUser          = new LoginUser(User);
                        let token              = loginUser.giveToken();
                            req.session.idUser = User._id
                        req.session.save(() => {
                            res.status(201).json({success: 'User Created', token: token});
                        });
                    })
                    .catch(err => {
                        res.status(500).json({ error: `Internal Server Error: User Creation Failed => ${err}`, code: err.code });
                    });
                }); 
            }
        } else {
            res.status(400).json({ error: 'Bad Request : Missing Parameters' });
        }
    });
};


exports.postLogin = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
    } else {
        const email    = req.body.email;
        const password = req.body.password;
        UserModel.findOne({ email: email })
            .then(user => {
                securite.compare(password, user.password)
                .then(bool => {
                    if(bool) {
                        let loginUser          = new LoginUser(UserModel);
                        let token              = loginUser.giveToken();
                            req.session.idUser = user._id
                        req.session.save(() => {
                            res.status(200).json({success: 'Login Ok.', token: token});
                        });
                    } else {
                        res.status(401).json({ error: 'Unauthorized: Bad Password.'});
                    }
                });
            })
            .catch(err => res.status(404).json({ error: 'User Not Found.', err: err }));
    }
};



var nJwt = require('njwt');
const variableSecret = require('../data/secret/variableSecret.js');
const secretJwt      = variableSecret.secretJWT;

exports.postForgotPassword = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
    } else {
        UserModel.findOne({ email: req.body.email })
        .then(result => {
            if(result == null) {
                res.status(404).json({ error: 'User Not Found.', err: err })
            } else {
                const sendEmail = new Email(
                    req.body.email,
                    'forgtopassword',
                    req.body.locale
                )
                //sendEmail.emailSend();
                nJwt.verify("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL3d3dy5wbWF1cnkuZnIiLCJ1c2VyIjoiIiwicm9sZSI6InVzZXIiLCJpYXQiOjE1ODA3NTE3MzAsImV4cCI6MTU4MDg0MjAwMH0.pbJct9vVJ3_Lm96fii_DjRO5tJXHUH2WkQkUOWLvsyU",secretJwt, 'HS256')
                //je créer un token qui dure genre 1h (mettre une valeur de défaut et un truc quand je précise)
                //envoie token avec l'url 
                //quand le mec click ça redirige etc
                //je réception le nouveau mdp avec la confirm et le jeton
                //verif que le mdp est bon + jeton valide
                //faire la modif
            }
        })
        .catch(err => res.status(500).json({ error: `Internal Server Error: User Creation Failed => ${err}`, code: err.code }));
    }
}