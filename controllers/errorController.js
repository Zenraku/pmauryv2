exports.getError = (err, req, res, next) => {
    if(err.name === 'UnauthorizedError') {
        res.status(404)
        res.sendFile(__dirname + '/dist/index.html');
      }
   next(err);
};