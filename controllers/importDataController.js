const countriesArray = require('../data/countries.js');
const statesArray    = require('../data/states.js');
const citiesArray    = require('../data/cities.js');

exports.importData = (err, db) => {

    if(err) throw err;
    let countries = db.collection("countries");
    countries.find({ name: "France" }).toArray((err, rslt) => {
        if(rslt[0] == undefined) {
            countries.insertMany(countriesArray, (err, res) => {
                if(err) console.log(`Une erreur est survenue lors de l'insertion des countries : ${err}`)
                else console.log(res.insertedCount + " documents inséré dans countries");
            });
        }
        else {
            console.log("Données countries déjà présentes.")
        }
    });

    let states = db.collection("states");
    states.find({ name: "Bouches-du-Rhone" }).toArray((err, rslt) => {
        if(rslt[0] == undefined) {
            states.insertMany(statesArray, (err, res) => {
                if(err) console.log(`Une erreur est survenue lors de l'insertion des states : ${err}`)
                else console.log(res.insertedCount + " documents inséré dans states");
            });
        }
        else {
            console.log("Données states déjà présentes.")
        }
    });

    let cities = db.collection("cities");
    cities.find({ name: "Martigues" }).toArray((err, rslt) => {
        if(rslt[0] == undefined) {
            cities.insertMany(citiesArray, (err, res) => {
                if(err) console.log(`Une erreur est survenue lors de l'insertion des cities : ${err}`)
                else console.log(res.insertedCount + " documents inséré dans cities");
            });
        }
        else {
            console.log("Données cities déjà présentes.")
        }
    });
}
