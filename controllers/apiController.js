const ApiModel  = require('../models/apiModel.js');
const UserModel = require('../models/userModel.js');
const ApiClass  = require('../class/apiClass.js');
const Securite  = require('../class/securiteClass.js');
const securite  = new Securite();


const express = require('express'),
      router  = express.Router();

exports.getDocs = (req, res, next) => {
    res.status(200).json({
        dateLastMaj: "30/01/2020",
        routes     : {
            type        : 'Routes (GET)',
            docs        : 'Documentation de l\'API : www.pmaury.fr/api/world_list/docs',
            countries   : 'Liste de tout les pays : www.pmaury.fr/api/world_list/countries?key={myKey}',
            states      : 'Liste de tout les états/départements : www.pmaury.fr/api/world_list/states?key={myKey}&country{idCountry}',
            cities      : 'Liste de toutes les villes : www.pmaury.fr/api/world_list/cities?key={myKey}&state={myState}',
            key         : 'Obtenir une Key APi : www.pmaury.fr/api/world_list/key?nameKey={Nom de L\'API}, ici world_list',
            forgotApiKey: 'Obtenir un Array des Keys de toutes les APIs d\'un`utilisateur (Nécessite l\'authentification sur le site)',
            deleteKey   : 'Supprimer une KEy d\'un utilisateur'
        },
        typeResponse: "JSON",
        key         : "Nécessaire pour utilisation de l'API (sauf documention et obtention de la key)"
    });
};

exports.getCountries = (req, res, next) => {

    let nameApi = req.originalUrl.split('/');
        nameApi = nameApi[2];

    const myApi = new ApiClass(req.session.idUser);
    myApi.validKey(req.query.key, nameApi)
    .then(bool => {
        if(bool) {

            myApi.findCollection("countries", {}, function(err, docs) {
                if(err) res.status(500).json({ error: `Internal Server Error: Can\t find data  => ${err}`, code: err.code });
                else {
                    res.status(200).json({ success: true, data: docs });
                }
            });
            
        } else {
            res.status(401).json({ error: `Unauthorized : Invalid Key` });
        }
    })
    .catch(err => {
        res.status(500).json({ error: `Internal Server Error: Key validation Failed => ${err}`, code: err.code });
    });
};

exports.getStates = (req, res, next) => {

    let   nameApi   = req.originalUrl.split('/');
          nameApi   = nameApi[2];
    const idCountry = req.query.country;

    const myApi = new ApiClass(req.session.idUser);
    myApi.validKey(req.query.key, nameApi)
    .then(bool => {
        if(bool) {
            
            myApi.findCollection("states", {country_id: idCountry}, function(err, docs) {
                if(err) res.status(500).json({ error: `Internal Server Error: Can\t find data => ${err}`, code: err.code });
                else {
                    res.status(200).json({ success: true, data: docs });
                }
            });
            
        } else {
            res.status(401).json({ error: `Unauthorized : Invalid Key` });
        }
    })
    .catch(err => {
        res.status(500).json({ error: `Internal Server Error: Key validation Failed => ${err}`, code: err.code });
    });
};

exports.getCities = (req, res, next) => {
    let   nameApi = req.originalUrl.split('/');
          nameApi = nameApi[2];
    const idState = req.query.state;

    const myApi = new ApiClass(req.session.idUser);
    myApi.validKey(req.query.key, nameApi)
    .then(bool => {
    if(bool) {
        
        myApi.findCollection("cities", {state_id: idState}, function(err, docs) {
            if(err) res.status(500).json({ error: `Internal Server Error: Can\t find data => ${err}`, code: err.code });
            else {
                res.status(200).json({ success: true, data: docs });
            }
        });
        
    } else {
        res.status(401).json({ error: `Unauthorized : Invalid Key` });
    }
    })
    .catch(err => {
    res.status(500).json({ error: `Internal Server Error: Key validation Failed => ${err}`, code: err.code });
    });
};

exports.getKey = (req, res, next) => {
    let nameKey = req.originalUrl.split('/');
        nameKey = nameKey[2];

    UserModel.findById(req.session.idUser)
         .then(user => {
            const myApi = new ApiClass(req.session.idUser);
            myApi.giveKey(nameKey);
            const Api = new ApiModel(myApi);
            ApiModel.findOne({ idUser: req.session.idUser,
                               'arrKeyApi.name': nameKey},
                             {'arrKeyApi.$': 1},
            (err, result) => {
                if(result !== null) {
                    res.status(403).json({error: 'Forbidden: Key akready exist'});
                } else {
                    for(let i = 0; i < Api.arrKeyApi.length; i++) {
                        
                        securite.cryptoEncrypt(Api.arrKeyApi[i].key)
                        .then(crypted => {
                            let           sendKey = Api.arrKeyApi[i].key;
                            Api.arrKeyApi[i].key  = crypted;
                         ApiModel.findOne({idUser: req.session.idUser})
                            .then(result => {  
                                if(result == null) {
                                    Api.save()
                                    .then(data => {
                                        res.status(201).json({success: 'Key Created', key: sendKey });
                                    })
                                    .catch(err => {
                                        res.status(500).json({ error: `Internal Server Error: Key Creation Failed => ${err}`, code: err.code });
                                    });
                                } else {
                                    ApiModel.updateOne(
                                        { idUser: req.session.idUser }, 
                                        { $push: { arrKeyApi: { name: nameKey, key: Api.arrKeyApi[i].key } } }
                                    ).then(() => {
                                        res.status(201).json({success: 'Key Created', key: sendKey });
                                    });
                                }
                            })
                            .catch((err) => {
                                res.status(500).json({ error: `Internal Server Error: Key Creation Failed => ${err}`, code: err.code });
                            })
                        })
                        .catch(err => res.status(500).json({ error: 'Internal Serveur Error.', err: err }));
                    }
                }
            });
         })
         .catch(err => res.status(404).json({ error: 'User Not Found.', err: err }));
};

exports.getForgotKey = (req, res, next) => {

    UserModel.findById(req.session.idUser)
    .then(() => {
        ApiModel.findOne({idUser: req.session.idUser}, (err, result) => {
            
            if(result == null) {
                res.status(404).json({ error: 'Api Key not Found.' });
            }
            else { 
                let arrayRslt = [];
                for(let v = 0; v < result.arrKeyApi.length; v++)
                {
                    securite.cryptoDecrypt(result.arrKeyApi[v].key)
                    .then(decrypted => {
                        result.arrKeyApi[v].key = decrypted;
                        arrayRslt.push({name: result.arrKeyApi[v].name, key: result.arrKeyApi[v].key});
                        if(v == (result.arrKeyApi.length-1)) {
                            res.status(200).json({success: 'Data Found.', data: arrayRslt });
                        }
                    })
                    .catch(err => {
                        res.status(500).json({err: `No Data Found. ${err}`});
                    })
                }
            }
        });
    })
    .catch(err => res.status(404).json({ error: 'User Not Found.', err: err }));
};

exports.deleteKey = (req, res, next) => {
    let nameApi = req.originalUrl.split('/');
        nameApi = nameApi[2];

    ApiModel.findOneAndUpdate(
        { idUser: req.session.idUser },
        { $pull: { 'arrKeyApi': { name: nameApi } } },
        (err, model) => {
            if(err) {
                res.status(500).json({err: `Internal Serveur Error. ${err}`, code: err.code });
            } else {
                res.status(200).json({success: true });
            }
        });
}