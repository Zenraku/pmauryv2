const ApiModel = require('../models/apiModel.js');
const Securite = require('../class/securiteClass.js');
const mongoose = require('mongoose');
const securite = new Securite();
const ref      = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
const length   = 64;

module.exports = class Api {
    constructor(idUser = '', arrKeyApi = []) {
        this.idUser    = idUser;
        this.arrKeyApi = arrKeyApi;
    }

    giveKey(name) {
        let randomAlphaString = '';
        for (let i = length; i > 0; --i) {
            randomAlphaString += ref[Math.round(Math.random() * (ref.length - 1))];
        }
        let bool = this.uniqueKey(randomAlphaString);
        if(bool) {
            this.arrKeyApi.push({ name: name, key: randomAlphaString });
        } else {
            this.giveKey(name);
        }
    }

    async uniqueKey(key) {
        let cryptedKey = await securite.cryptoEncrypt(key);
        if(cryptedKey) {
            let unique = await ApiModel.findOne({'arrKeyApi.key' : cryptedKey}, {'arrKeyApi.$': 1}).exec();
            if(unique) {
                return false;
            } else {
                return true;
            }
        } else {
            return { error: `Internal Server Error: Key crypt Failed`, code: 11000 };
        }
    }

    async validKey(key, nameAPi) {

        let cryptedKey = await securite.cryptoEncrypt(key);
        if(cryptedKey) {
            let rslt = await ApiModel.findOne({idUser: this.idUser,
                'arrKeyApi.name': nameAPi,
                'arrKeyApi.key' : cryptedKey},
                {'arrKeyApi.$': 1}).exec();
    
            if(rslt !== null) {
                return true;
            } else {
                return false;
            }    
        } else {
            return { error: `Internal Server Error: Key crypt Failed`, code: 11000 };
        }
    }

    findCollection(collec, query, callback) {
        mongoose.connection.db.collection(collec, function (err, collection) {
            collection.find(query).toArray(callback);
        });
    }
}