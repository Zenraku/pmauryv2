module.exports = class SecuriteUser {
    constructor(email, password, confirmPassword, dateCreate) {
        this.email           = email;
        this.password        = password;
        this.confirmPassword = confirmPassword;
        this.dateCreate      = dateCreate;
    }

    async checkEmpty() {
        if((this.email === undefined || this.email == '') ||
           (this.password === undefined || this.password == '') || 
           (this.confirmPassword  === undefined || this.confirmPassword == '')) {
            return false;
        } else { return true; }
    }
}