const jwt            = require('jsonwebtoken');
const variableSecret = require('../data/secret/variableSecret.js');
const secret         = variableSecret.secretJWT;
const url            = 'https://www.pmaury.fr';

module.exports = class LoginUser {
    constructor({email = '', password = '', dateCreate = ''} = {}, role = 'user') {
        this.email      = email;
        this.password   = password;
        this.dateCreate = dateCreate;
        this.role       = role;
    }

    giveToken() {
        let   user  = this.email.split('@');
        const token = jwt.sign({
            iss : url,
            user: user[0],
            role: this.role,
        }, secret, { expiresIn: 172800 }); //2 days
        return token;
    }
}