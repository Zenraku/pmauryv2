const bcrypt         = require('bcryptjs');
const crypto         = require('crypto');
const variableSecret = require('../data/secret/variableSecret.js')
const algorithm      = 'aes256';

module.exports = class Securite {

    async encrypt(p) {
        let salt = await bcrypt.genSalt(10);
            p    = await bcrypt.hash(p, salt);
            return p;
    }

    async compare(password, hash) {
        let compare = await bcrypt.compare(password, hash);
        return compare;
    }

    async cryptoEncrypt(text) {
        let cipher   = await crypto.createCipher(algorithm, variableSecret.secretSecurite);
        let crypted  = await cipher.update(text,'utf8','hex');
            crypted += cipher.final('hex');
        return crypted;
    }

    async cryptoDecrypt(crypted) {
        let decipher  = await crypto.createDecipher(algorithm, variableSecret.secretSecurite);
        let dec       = await decipher.update(crypted,'hex','utf8');
            dec      += decipher.final('utf8');
        return dec;
    }
}