const variableSecret    = require('../data/secret/variableSecret.js');
const emailtemplate     = require('../data/template/emailTemplate.js');
const nodeMailer        = require('nodemailer');
const sendGridTransport = require('nodemailer-sendgrid-transport');
const transporter       = nodeMailer.createTransport(sendGridTransport({
    auth: {
        api_key: variableSecret.secretEmailKeyAPi
    }
}));

module.exports = class Email {
    constructor(to, html, lang = 'fr', from = 'contact@pmaury.fr', subject = '') {
        if   (html === 'signup') this.html = emailtemplate.templateSignUp(lang);
        else if(html === 'forgtopassword') this.html = emailtemplate.templateForgotPassword(lang);

        this.to      = to;
        this.from    = from;
        this.lang    = lang;

        if(this.lang == 'fr') this.subject = 'Demande mot de passe oublié.';
        else if(this.lang == 'en') this.subject = 'Request forgotten password.';
    }

    emailSend() {
        return transporter.sendMail({
            to     : this.to,
            from   : this.from,
            subject: this.subject,
            html   : this.html
        });
    }
}

