const configFix     = ['/', 
                       '/signup', 
                       '/login', 
                       '/forgotApiKey', 
                       '/forgotPassword'];
const configDynamic = ['/api/'];
let   publicRoutes  = [];

module.exports.setPublicRoutes = function setPublicRoutes (allRoutes) {
    allRoutes.forEach(element => {
        for(let w = 0; w < configFix.length; w++)
        {
            if(configFix[w] === element.path) {
                publicRoutes.push(configFix[w]);
            }
        }
        for(let z = 0; z < configDynamic.length; z++)
        {
            if((/(?:\/api\/)/g).test(element.path))
            {
                publicRoutes.push(element.path);
            }
        }
    });
    return publicRoutes;
}

module.exports.publicRoutes = publicRoutes;

//Routes où l'auth n'est pas obligatoire 
//Api car accessible en dehors du site