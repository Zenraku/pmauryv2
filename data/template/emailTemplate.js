const messages = {
  fr: {
    //GLOBAL
    'youReceivedEmail': 'Vous avez reçu cet e-mail car vous avez demandé une réinitialisation du mot de passe. Si vous ne l\'avez pas fait, ',
    'pleaseContactUs' : 'contactez-nous s\'il vous plait.',
    'adress'          : '18 Les Jardins d\'Hestia, 2 Impasse des Rayettes, 1500 Martigues, France',
    //Forgot Password
    //1er Partie
    'troubleSignIn': 'Difficultés à se connecter ?',
    'resetText'    : `La réinitialisation de votre mot de passe est facile. 
    Appuyez simplement sur le bouton ci-dessous et suivez les instructions.`,
    'resetButton': 'Réinitialiser le mot de passe',
    //2eme Partie
    'unable'     : 'Impossible de cliquer sur le bouton ci-dessus ?',
    'unableClick': 'Cliquez sur le lien ci-dessous ou copiez / collez dans la barre d\'adresse.',
    'unableLink' : 'Découvrez à quel point il est facile de commencer.',
    //3eme Partie
    'help'    : 'Besoin de plus d\'aide ?',
    'helpLink': 'Nous sommes ici, prêts à parler.'
  },
  en: {
    //GLOBAL
    'youReceivedEmail': 'You received this email because you requested a password reset. If you did not, ',
    'pleaseContactUs' : 'please contact us.',
    'adress'          : '18 Les Jardins d\'Hestia, 2 Impasse des Rayettes, 1500 Martigues, France',
    //Forgot Password
    //1er Partie
    'troubleSignIn': 'Trouble signing in ?',
    'resetText'    : `Resetting your password is easy. 
    Just press the button below and follow the instructions.`,
    'resetButton': 'Reset Password',
    //2eme Partie
    'unable'     : 'Unable to click on the button above ?',
    'unableClick': 'Click on the link below or copy/paste in the address bar.',
    'unableLink' : 'See how easy it is to get started.',
    //3eme Partie
    'help'    : 'Need more help ?',
    'helpLink': 'We’re here, ready to talk.'
  }
}

module.exports.templateForgotPassword = (lang) => {
  return `
  <!DOCTYPE html>
  <html>
  <head>
  <title></title>
  <meta  http-equiv = "Content-Type" content    = "text/html; charset=utf-8" />
  <meta  name       = "viewport" content        = "width=device-width, initial-scale=1">
  <meta  http-equiv = "X-UA-Compatible" content = "IE=edge" />
  <style type       = "text/css">
    /* FONTS */
      @media screen {
      @font-face {
        font-family: 'Lato';
        font-style : normal;
        font-weight: 400;
        src        : local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
      }
      
      @font-face {
        font-family: 'Lato';
        font-style : normal;
        font-weight: 700;
        src        : local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
      }
      
      @font-face {
        font-family: 'Lato';
        font-style : italic;
        font-weight: 400;
        src        : local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
      }
      
      @font-face {
        font-family: 'Lato';
        font-style : italic;
        font-weight: 700;
        src        : local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
      }
      }
      
      /* CLIENT-SPECIFIC STYLES */
      body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
      table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
      img { -ms-interpolation-mode: bicubic; }
  
      /* RESET STYLES */
      img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
      table { border-collapse: collapse !important; }
      body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }
  
      /* iOS BLUE LINKS */
      a[x-apple-data-detectors] {
          color          : inherit !important;
          text-decoration: none !important;
          font-size      : inherit !important;
          font-family    : inherit !important;
          font-weight    : inherit !important;
          line-height    : inherit !important;
      }
  
      /* ANDROID CENTER FIX */
      div[style*="margin: 16px 0;"] { margin: 0 !important; }
  </style>
  </head>
  <body style = "background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">
  
  <!-- HIDDEN PREHEADER TEXT -->
  <div style = "display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Lato', Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
      Looks like you tried signing in a few too many times. Let's see if we can get you back into your account.
  </div>
  
  <table border = "0" cellpadding = "0" cellspacing = "0" width = "100%">
      <!-- LOGO -->
      <tr>
          <td    bgcolor = "#2D85FF" align = "center">
          <table border  = "0" cellpadding = "0" cellspacing = "0" width = "480" >
                  <tr>
                      <td  align = "center" valign            = "top" style                                                                                                                              = "padding: 40px 10px 40px 10px;">
                      <a   href  = "http://pmaury.fr/" target = "_blank">
                      <img alt   = "Logo" src                 = "https://lh3.googleusercontent.com/proxy/6-QSJ_pOfld8qdc4xGkjNeNqxb08Qa8dDhx4f6aOfqZNkiPoJ2VpqrxuhRm91jRHA5aNt6Zq13dCskjleMicO3i4" width = "100" height = "100" style = "display: block;  font-family: 'Lato', Helvetica, Arial, sans-serif; color: #ffffff; font-size: 18px;" border = "0">
                          </a>
                      </td>
                  </tr>
              </table>
          </td>
      </tr>
      <!-- HERO -->
      <tr>
          <td    bgcolor = "#2D85FF" align = "center" style  = "padding: 0px 10px 0px 10px;">
          <table border  = "0" cellpadding = "0" cellspacing = "0" width = "480" >
                  <tr>
                      <td bgcolor = "#ffffff" align = "center" valign = "top" style = "padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;">
                      <h1 style   = "font-size: 32px; font-weight: 400; margin: 0;">`+messages[lang].troubleSignIn+`</h1>
                      </td>
                  </tr>
              </table>
          </td>
      </tr>
      <!-- COPY BLOCK -->
      <tr>
          <td    bgcolor = "#f4f4f4" align = "center" style  = "padding: 0px 10px 0px 10px;">
          <table border  = "0" cellpadding = "0" cellspacing = "0" width = "480" >
                <!-- COPY -->
                <tr>
                  <td bgcolor = "#ffffff" align = "left" style = "padding: 20px 30px 40px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;" >
                  <p  style   = "margin: 0;">`+messages[lang].resetText+`</p>
                  </td>
                </tr>
                <!-- BULLETPROOF BUTTON -->
                <tr>
                  <td    bgcolor = "#ffffff" align = "left">
                  <table width   = "100%" border   = "0" cellspacing = "0" cellpadding = "0">
                      <tr>
                        <td    bgcolor = "#ffffff" align = "center" style  = "padding: 20px 30px 60px 30px;">
                        <table border  = "0" cellspacing = "0" cellpadding = "0">
                            <tr>
                                <td align = "center" style = "border-radius: 3px;" bgcolor = "#2D85FF"><a href = "https://litmus.com" target = "_blank" style = "font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; padding: 15px 25px; border-radius: 2px; border: 1px solid #2D85FF; display: inline-block;">`+messages[lang].resetButton+`</a></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
          </td>
      </tr>
      <!-- COPY CALLOUT -->
      <tr>
          <td    bgcolor = "#f4f4f4" align = "center" style  = "padding: 0px 10px 0px 10px;">
          <table border  = "0" cellpadding = "0" cellspacing = "0" width = "480" >
                  <!-- HEADLINE -->
                  <tr>
                    <td bgcolor = "#111111" align = "left" style = "padding: 40px 30px 20px 30px; color: #ffffff; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;" >
                    <h2 style   = "font-size: 24px; font-weight: 400; margin: 0;">`+messages[lang].unable+`</h2>
                    </td>
                  </tr>
                  <!-- COPY -->
                  <tr>
                    <td bgcolor = "#111111" align = "left" style = "padding: 0px 30px 20px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;" >
                    <p  style   = "margin: 0;">`+messages[lang].unableClick+`</p>
                    </td>
                  </tr>
                  <!-- COPY -->
                  <tr>
                    <td bgcolor = "#111111" align      = "left" style               = "padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;" >
                    <p  style   = "margin: 0;"><a href = "http://litmus.com" target = "_blank" style = "color: #2D85FF;">`+messages[lang].unableLink+`</a></p>
                    </td>
                  </tr>
              </table>
          </td>
      </tr>
      <!-- SUPPORT CALLOUT -->
      <tr>
          <td    bgcolor = "#f4f4f4" align = "center" style  = "padding: 30px 10px 0px 10px;">
          <table border  = "0" cellpadding = "0" cellspacing = "0" width = "480" >
                  <!-- HEADLINE -->
                  <tr>
                    <td bgcolor = "#95C2FF" align      = "center" style             = "padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;" >
                    <h2 style   = "font-size: 20px; font-weight: 400; color: #111111; margin: 0;">`+messages[lang].help+`</h2>
                    <p  style   = "margin: 0;"><a href = "http://litmus.com" target = "_blank" style = "color: #2D85FF;">`+messages[lang].helpLink+`</a></p>
                    </td>
                  </tr>
              </table>
          </td>
      </tr>
      <!-- FOOTER -->
      <tr>
          <td    bgcolor = "#f4f4f4" align = "center" style  = "padding: 0px 10px 0px 10px;">
          <table border  = "0" cellpadding = "0" cellspacing = "0" width = "480" >
                
                <!-- PERMISSION REMINDER -->
                <tr>
                  <td bgcolor = "#f4f4f4" align                                                                             = "left" style = "padding: 0px 30px 30px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 18px;" >
                  <p  style   = "margin: 0;">`+messages[lang].youReceivedEmail+` <a href       = "http://litmus.com" target = "_blank" style = "color: #111111; font-weight: 700;">`+messages[lang].pleaseContactUs+`</a>.</p>
                  </td>
                </tr>
                
                <!-- ADDRESS -->
                <tr>
                  <td bgcolor = "#f4f4f4" align  = "left" style = "padding: 0px 30px 30px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 18px;" >
                  <p  style   = "margin: 0;">`+messages[lang].adress+`</p>
                  </td>
                </tr>
              </table>
          </td>
      </tr>
  </table>
  
  </body>
  </html>
  
  `;
} 

exports.templateSignUp = `

`;
