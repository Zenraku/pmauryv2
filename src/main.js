//Global
import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import i18n from './i18n';
import VueParticles from 'vue-particles';
import vmodal from 'vue-js-modal';
import '@fortawesome/fontawesome-free/css/all.css';
import '@fortawesome/fontawesome-free/js/all.js';

//Component
import Home from '@/components/routes/Home.vue';
import PageNotFound from '@/components/routes/PageNotFound.vue';
import NotSupported from '@/components/routes/NotSupported.vue';


//Use
Vue.use(vmodal)
Vue.use(VueParticles);
Vue.use(VueRouter);

Vue.config.productionTip = false;

const router = new VueRouter({
  routes: [
    { 
      path: '/',
      name: "Home",
      component: Home 
    },
    { 
      path: '/Browser',
      name: "NotSupported",
      component: NotSupported
    },
    { 
      path: '*',
      name: "PageNotFound",
      component: PageNotFound 
    },
  ],
  mode: 'history',
  base: process.env.BASE_URL
});

new Vue({
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')
